// 配置路由规则
import VueRouter from 'vue-router'
//引入组件
import BodyBase from '../pages/BodyBase'
import BodyTwo from '../pages/BodyTwo'
import three from '../pages/three'
import threece from '../detail/threece'
import adidas from '../detail/adidas'
import akf from '../detail/akf'
import lining from '../detail/lining'
import ugg from '../detail/ugg'
import ListAa from '../pages/ListAa'

//创建并暴露一个路由器
export default new VueRouter({
    routes: [
        {
            path: '/',
            component: BodyBase,
            //     children: [
            //         {
            //             path: '3ce',
            //             component: threece
            //         }
            //     ]
        },
        {
            path: '/base',
            component: BodyBase,
        },
        {
            path: '/two',
            component: BodyTwo
        },
        {
            path: '/three',
            component: three
        },
        {
            path: '/3ce',
            component: threece
        },
        {
            path: '/adidas',
            component: adidas
        },
        {
            path: '/list',
            component: ListAa
        },
        {
            path: '/ugg',
            component: ugg
        },
        {
            path: '/lining',
            component: lining
        },
        {
            path: '/akf',
            component: akf
        }
    ]
})

// export default router