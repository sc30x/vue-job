import Vuex from 'vuex';

import Vue from 'vue';
import state from './state';
import actions from './actions';
import mutations from './mutations'
import user from './mudules/user'
Vue.use(Vuex);

const store = new Vuex.Store({
     state,
     mutations,
     actions,
     getters:{
       getBuyNums(state){
          return state.buyNums;
       }
     },
     modules:{// 每个模块对象都有自己的 state mutation action getters
        user
     }
 
  })

  export default store;