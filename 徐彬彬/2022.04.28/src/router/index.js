// 配置路由规则
import VueRouter from 'vue-router'
//引入组件
import BodyBase from '../pages/BodyBase'
import BodyTwo from '../pages/BodyTwo'
import three from '../pages/three'
import threece from '../detail/threece'
import adidas from '../detail/adidas'
import akf from '../detail/akf'
import lining from '../detail/lining'
import ugg from '../detail/ugg'
import ListAa from '../pages/ListAa'
import shoppingcart from '../pages/ShoppingCart'

//创建并暴露一个路由器
export default new VueRouter({
    routes: [
        {
            path: '/',
            component: BodyBase,
            //     children: [
            //         {
            //             path: '3ce',
            //             component: threece
            //         }
            //     ]
        },
        {
            path: '/base',
            component: BodyBase,
        },
        {
            path: '/two',
            component: BodyTwo
        },
        {
            path: '/three',
            component: three
        },
        {
            name: '3ce',
            path: '/3ce/:id/:price/:checkbox',
            component: threece,
            props: true
        },
        {
            path: '/adidas',
            component: adidas
        },
        {
            name: 'list',
            path: '/list/:id/:name/:price',
            component: ListAa,
            props: true
        },
        {
            path: '/ugg',
            component: ugg
        },
        {
            path: '/lining',
            component: lining
        },
        {
            path: '/akf',
            component: akf
        },
        // {
        //     path: '/cart',
        //     component: shoppingcart
        // },
        {
            name: '/cart',
            path: '/cart',
            component: shoppingcart,
            props: true
        }
    ]
})

// export default router