import axios from 'axios';
const mutations = {
  changeBuyNum(state, num) {
    console.log('change buynum');
    // console.log(state);
    //请求后台数据,
    axios.get('https://question.llblog.cc/api.php').then(res => {
      console.log(res);
      state.buyNums = state.buyNums + num; //0+1    1+1    0
    });

  },
  // 加入购物车
  addToCart(state, product) {
    let inCart = false;
    // console.log(product);
    state.cart.forEach(el => {
      if (el.id == product.id) {
        el.num++
        inCart = true;
      }
    })
    if (!inCart) {
      state.cart.push(product);
    }
  },
  // 购物车商品数量减少
  cartjian(state, id) {
    state.cart.forEach(e => {
      if (id == e.id) {
        e.num--
      }
    });
  },
  // 购物车商品数量增加
  cartjia(state, id) {
    state.cart.forEach(e => {
      if (id == e.id) {
        e.num++
      }
    });
  },
  // 商品部分选中
  cartchecked(state, date) {
    state.cart[date.index].checkbox = !state.cart[date.index].checkbox
    this.checkbox = state.cart.some(el => el.checkbox)
    // 商品数量加减
    if (state.cart[date.index].checkbox == true) {
      state.cartnum++
    } else {
      state.cartnum--
    }
    // 购物车数量等于已选数量时全选
    let length = state.cart.length;
    if (length == state.cartnum) {
      state.isCheckedAll = true
    } else {
      state.isCheckedAll = false
    }
  },
  // 商品全选
  cartcheckAll(state) {
    state.isCheckedAll = !state.isCheckedAll
    state.cart.map(el => el.checkbox = state.isCheckedAll)
    if (state.isCheckedAll) {
      state.cartnum = state.cart.length
    } else {
      state.cartnum = 0
    }
  }
}

export default mutations;