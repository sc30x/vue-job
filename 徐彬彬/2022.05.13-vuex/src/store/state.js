const state = {
    cart: [],//购物车中的商品
    cartnum:0,//购物车选中数量
    isCheckedAll: false,//购物车全选状态
    buyNums: 1,//购买的数量，每次点击购买时，把对应购买数量+1
    shangpin: [
        { id: 1, name: '韩国三熹玉3CE彩妆香水专场', checkbox: false, img: '../assets/header.jpg', count: '2.7', price: 240, num: 1, img2: '../../assets/3ce.webp' },
        { id: 2, name: '阿迪达斯三叶草运动户外专场', checkbox: false, img: '../assets/center.jpg', count: '3.6', price: 300, num: 1, img2: '../../assets/adidas.webp' },
        { id: 3, name: 'UGG专场', checkbox: false, img: '../assets/bottom.jpg', count: '5.7', price: 232, num: 1, img2: '../../assets/ugg.webp' },
        { id: 4, name: '伊芙丽eifini女装专场', checkbox: false, img: '../assets/yfl.jpg', count: '5.7', price: 456, num: 1, img2: '../../assets/akf.webp' }
    ],
    shangpin2: [
        { id: 5, name: 'AKF护肤彩妆专场', img: '../assets/akf.jpg', num: '2.7', price: 986 },
        { id: 6, name: '李宁LI-NING运动户外专场', img: '../assets/ln.jpg', num: '3.6', price: 157 },
        { id: 7, name: '薇诺娜winona面部护理专场', img: '../assets/wnn.jpg', num: '5.7', price: 652 },
        { id: 8, name: '伊芙丽eifini女装专场', img: '../assets/yfl.jpg', num: '5.7', price: 423 }
    ]

}
export default state;
