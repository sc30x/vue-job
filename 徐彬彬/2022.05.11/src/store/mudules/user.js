const user={
    namespaced: true,
     state:{
          userInfo:{name:'vue',age:5},
     },
     mutations:{
       changeUserInfo(state,payload){
         console.log('userInfo的changeUserInfo');
         state.userInfo=payload
       }
     },
     actions:{
       changeUserInfoByAction({commit},payload){// context commit
         console.log('userInfo的changeUserInfoByActio');
         commit('changeUserInfo',payload);
         //联动的操作，还要把全局的name给修改
         //因为 name 是全局的，官网提供了 他们可以接受 `root` 属性以访问根 dispatch 或 commit
         commit('changeName','区块链',{root: true});
       }
     }

  }

  export default user;