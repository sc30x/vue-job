import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import VueScroller from 'vue-scroller'
Vue.use(VueScroller)

//引入VueRouter
import VueRouter from 'vue-router'
//应用插件
Vue.use(VueRouter)
Vue.use(Vuex)

const store = new Vuex.Store({
  state:{
    title:'外卖系统',
    price:100,
    shangpin:[
      {id:1,name:'韩国三熹玉3CE彩妆香水专场',checkbox: false,img:'../assets/header.jpg',count:'2.7',price:240,num:1,img2:'../../assets/3ce.webp'},
      {id:2,name:'阿迪达斯三叶草运动户外专场',checkbox: false,img:'../assets/center.jpg',count:'3.6',price:300,num:1,img2:'../../assets/adidas.webp'},
      {id:3,name:'UGG专场',checkbox: false,img:'../assets/bottom.jpg',count:'5.7',price:232,num:1,img2:'../../assets/ugg.webp'},
      {id:4,name:'伊芙丽eifini女装专场',checkbox: false,img:'../assets/yfl.jpg',count:'5.7',price:456,num:1,img2:'../../assets/akf.webp'}
  ]
  },
  mutations:{
    changeTile(state,payload){
      state.title = payload
    }
  }
})

//引入路由器
import router from './router/index'

//关闭生产环境提交
Vue.config.productionTip = false
//render 渲染 temldate
new Vue({
  el:'#app',
  // template:'<App></App>',
  // components:{
  //   App,
  // }
  //脚手架引入的vue是残缺的，为了性能
  render: (createElement) => {
    // console.log(typeof createElement);
    return createElement(App)
   },
   router,
   store
})
